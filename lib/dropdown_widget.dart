import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  DropDownWidget({Key? key}) : super(key: key);

  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: Row(
              children: [
                Text('Vaccine'),
                Spacer(),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                    DropdownMenuItem(
                        child: Text('Johnson & Johnson'),
                        value: 'Johnson & Johnson'),
                    DropdownMenuItem(
                        child: Text('Sputnik V'), value: 'Sputnik V'),
                    DropdownMenuItem(
                        child: Text('AstraZeneca'), value: 'AstraZeneca'),
                    DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
                    DropdownMenuItem(
                        child: Text('Sinopharm'), value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
                  ],
                  value: vaccine,
                  onChanged: (String? value) {
                    setState(() {
                      vaccine = value!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              'result: $vaccine',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}
